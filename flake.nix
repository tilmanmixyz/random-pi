{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };
  outputs = {self,nixpkgs}:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in
      {
        packages.${system}.default = pkgs.rustPlatform.buildRustPackage {
          pname = "random-pi";
          version = "0.0.1";
          src = ./.;
          cargoLock = {
            lockFile = ./Cargo.lock;
          };
        };
        devShells.${system}.default = pkgs.mkShell {
          nativeBuildInputs = [
            pkgs.pkg-config
          ];
          packages = with pkgs; [
            bacon
          ];
          buildInputs = with pkgs; [
            cargo
          ];
        };
      };
}

use polars::prelude::*;

fn main() -> PolarsResult<()> {
    let mut args = std::env::args();
    let Some(csv) = args.nth(1) else {
        println!("error: no file input found\nusage: bench <input_file as csv>\n");
        std::process::exit(1);
    };
    let csv = std::path::PathBuf::from(csv);

    let csv_data = CsvReader::from_path(csv)?
        .has_header(true)
        .finish()
        .unwrap();

    let mean = csv_data.mean();
    println!("{mean}");

    Ok(())
}

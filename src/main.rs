use std::io::Write;

const POINTS_IN_SQUARE: usize = i32::MAX as usize;

fn main() -> std::io::Result<()> {
    let single_time = std::time::Instant::now();
    {
        let points_in_circle = random_pi::random_points(POINTS_IN_SQUARE);
        let pi = random_pi::calc_pi(points_in_circle, POINTS_IN_SQUARE);
        println!("Pi is about equal to {:.5}", pi);
    }
    let single_took = single_time.elapsed().as_millis();
    let multi_time = std::time::Instant::now();
    {
        let points_in_circle = random_pi::random_points_multi(POINTS_IN_SQUARE, Some(100));
        let pi = random_pi::calc_pi(points_in_circle, POINTS_IN_SQUARE);
        println!("Pi is about equal to {:.5}", pi);
    }
    let multi_took = multi_time.elapsed().as_millis();
    let channel_time = std::time::Instant::now();
    {
        let points_in_circle = random_pi::random_points_channel(POINTS_IN_SQUARE, Some(100));
        let pi = random_pi::calc_pi(points_in_circle, POINTS_IN_SQUARE);
        println!("Pi is about equal to {:.5}", pi);
    }
    let channel_took = channel_time.elapsed().as_millis();
    println!("Single threaded took {}ms", single_took);
    println!("Multi threaded took {}ms", multi_took);
    println!("Channel took {}ms", channel_took);

    // let mut time_csv = std::fs::File::create("time.csv")?;
    // time_csv.write("single,mutex,channel\n".as_bytes())?;

    // for _ in 0..100 {
    //     let single_start = std::time::Instant::now();
    //     let points_in_circle = random_pi::random_points(POINTS_IN_SQUARE);
    //     let _ = random_pi::calc_pi(points_in_circle, POINTS_IN_SQUARE);
    //     let single_took = single_start.elapsed().as_millis().to_string();

    //     time_csv.write(single_took.as_bytes())?;
    //     time_csv.write(",".as_bytes())?;

    //     let mutex_start = std::time::Instant::now();
    //     let points_in_circle = random_pi::random_points_multi(POINTS_IN_SQUARE, None);
    //     let _ = random_pi::calc_pi(points_in_circle, POINTS_IN_SQUARE);
    //     let mutex_took = mutex_start.elapsed().as_millis().to_string();

    //     time_csv.write(mutex_took.as_bytes())?;
    //     time_csv.write(",".as_bytes())?;

    //     let channel_start = std::time::Instant::now();
    //     let points_in_circle = random_pi::random_points_channel(POINTS_IN_SQUARE, None);
    //     let _ = random_pi::calc_pi(points_in_circle, POINTS_IN_SQUARE);
    //     let channel_took = channel_start.elapsed().as_millis().to_string();

    //     time_csv.write(channel_took.as_bytes())?;
    //     time_csv.write("\n".as_bytes())?;
    // }

    Ok(())
}

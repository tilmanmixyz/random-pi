use std::sync::{Arc, Mutex};

const MAX_THREADS: usize = 8;

/// Calculate the points which are in circle singlethreaded
/// ```
/// random_pi::random_points(1);
/// // Or with custom number of threads
/// random_pi::random_points(1);
/// ```
pub fn random_points(points: usize) -> usize {
    (0..points)
        .map(|_| {
            let pos: (f64, f64) = (
                fastrand::f64(),
                fastrand::f64(),
            );

            if pos.0.powf(2.0) + pos.1.powf(2.0) < 1.0 {
                return 1;
            } else {
                return 0;
            }
        })
        .sum()
}

/// Calculate the points which are in circle multithreaded
/// If `None` is provided as second argument 8 threads are being used
/// ```
/// random_pi::random_points_multi(1, None);
/// // Or with custom number of threads
/// random_pi::random_points_multi(1, Some(10));
/// ```
pub fn random_points_multi(points: usize, max_threads: Option<usize>) -> usize {
    let max_threads = max_threads.unwrap_or(MAX_THREADS);
    let mut handles = Vec::with_capacity(max_threads);
    let counter = Arc::new(Mutex::new(0 as usize));

    for _ in 0..max_threads {
        let counter = Arc::clone(&counter);
        let handle = std::thread::spawn(move || {
            let mut points_in_circle: usize = 0;
            for _ in 0..(points / max_threads) {
                let pos: (f64, f64) = (
                    fastrand::f64(),
                    fastrand::f64(),
                );

                if (pos.0.powf(2.0) + pos.1.powf(2.0)) < 1.0 {
                    points_in_circle += 1;
                }
            }
            let mut num = counter.lock().unwrap();
            *num += points_in_circle;
        });

        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }
    let points = counter.lock().unwrap();
    *points
}

pub fn random_points_channel(points: usize, max_threads: Option<usize>) -> usize {
    let max_threads = max_threads.unwrap_or(MAX_THREADS);
    let (sender, receiver) = std::sync::mpsc::sync_channel(max_threads);

    for _ in 0..max_threads {
        let sender = sender.clone();

        std::thread::spawn(move || {
            let mut points_in_circle: usize = 0;
            for _ in 0..(points / max_threads) {
                let pos: (f64, f64) = (
                    fastrand::f64(),
                    fastrand::f64(),
                );

                if (pos.0.powf(2.0) + pos.1.powf(2.0)) < 1.0 {
                    points_in_circle += 1;
                }
            }

            sender.send(points_in_circle).unwrap();
        });
    }

    let points = (0..max_threads).map(|_| receiver.recv().unwrap()).sum();
    points
}

/// Approximate Pi
pub fn calc_pi(in_circle: usize, in_square: usize) -> f64 {
    4.0 * in_circle as f64 / in_square as f64
}
